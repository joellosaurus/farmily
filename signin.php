<?php
    if (!isset($_SERVER['PHP_AUTH_USER']) &&
        !isset($_SERVER['PHP_AUTH_PW']) ) {
        header('HTTP/1.1 401 Unauthorized');
        header('WWW-Authenticate: Basic realm="localhost"');
        echo 'Text to send if user hits Cancel button';
        exit;
    } else {
        /* THE CONNECTION SHOULD BE DONE ONLY ONCE AND NOT ON EACH PAGE */
        //connect to the database
        @ $db = new mysqli( "localhost", "joel", "lavatwilight4", "farmily");
        //check the connection
        if (mysqli_connect_errno()) {
            echo 'Error: Could not connect to database.  Please try again later.';
            exit;
        }

        // get the username
        $username = $db->real_escape_string(trim($_SERVER['PHP_AUTH_USER']));
        // get the password
        $password = $db->real_escape_string(htmlspecialchars($_SERVER['PHP_AUTH_PW']));
        
        if(! $username || ! $password) {
            header('HTTP/1.1 401 Unauthorized');
            header('WWW-Authenticate: Basic realm="localhost"');
            exit("You need to fill in both the username and password.");
            
        }
            
        
        
        // Look up the user-provided credentials
        // find all the user names = $username

        // make sure password has a hash
        
        $query = "SELECT userID, userName, password FROM user WHERE userName = '" . $username . "'";
        $result = $db->query($query);
        $found = false;
        while ($row = $result->fetch_assoc()) {
            if (password_verify ($password, $row['password']))  {
                $found = true;
                echo $row['password'];
                break;
            }
        }
        
        if ($found) {
            $row = $result->fetch_assoc();
            $current_userID = $row['userID'];
            $current_userName = $row['userName'];
            echo $current_userName;
            echo $current_userID;
        } else {
            header('HTTP/1.1 401 Unauthorized');
            header('WWW-Authenticate: Basic realm="localhost"');
            exit("You need a valid username and password."); 
        }
        
        $result->free();
        $db->close();
        
    }
?>
<html xmlns:mso="urn:schemas-microsoft-com:office:office" xmlns:msdt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"><head>
<!--[if gte mso 9]><xml>
<mso:CustomDocumentProperties>
<mso:_dlc_DocId msdt:dt="string">YA3S45XA77AD-11-90</mso:_dlc_DocId>
<mso:_dlc_DocIdItemGuid msdt:dt="string">86e97fa7-1ef0-4f35-aff8-e25a287359cb</mso:_dlc_DocIdItemGuid>
<mso:_dlc_DocIdUrl msdt:dt="string">https://vle.anglia.ac.uk/modules/2013/MOD002701/TRI2-C-1/_layouts/DocIdRedir.aspx?ID=YA3S45XA77AD-11-90, YA3S45XA77AD-11-90</mso:_dlc_DocIdUrl>
</mso:CustomDocumentProperties>
</xml><![endif]-->
</head>