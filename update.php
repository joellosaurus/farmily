<?php
require_once 'core/init.php';

$user = new User();

if(!$user->isLoggedIn()) {
	Redirect::to('test.php');
}

if(Input::exists()) {
	if(Token::check(Input::get('token'))) {

		$validate = new Validate();
		$validation = $validate->check($_POST, array(
			'fname' => array(
				'required' => true,
				'min' => 1,
				'max' => 50
				)
			));

		if($validation->passed()) {
			try {
				$user->update(array(
					'fname' => Input::get('fname')
					));

			Session::flash('home', 'Details updated.');
			Redirect::to('test.php');

			} catch(Exception $e) {
				die($e->getMessage());

			}

		} else {
			foreach($validation->errors() as $error) {
				echo $error, '<br>';

			}
		}
	}
}

?>

<form action="" method="post">
	<div class="field">
		<label for="fname">Name</label>
		<input type="text" name="fname" value="<?php echo htmlspecialchars($user->data()->fname); ?>">
		<input type="submit" value="Update">
		<input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
	</div>
</form>