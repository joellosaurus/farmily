<?php
session_start();

$GLOBALS['config'] = array(
	'mysql' => array(
		'host' => 'localhost',
		'username' => 'joel',
		'password' => 'lavatwilight4',
		'db' => 'farmily'
		),

	'remember' => array(
		'cookie_name' => 'hash',
		'cookie_expiry' => 604800 
		),

	'session' => array(
		'session_name' => 'user',
		'token_name' => 'token'
		)
	);

// currency 

$currency = '&pound'; 
$current_url = base64_encode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);

// paypal

$PayPalMode         = 'sandbox'; // sandbox or live
$PayPalApiUsername  = 'admin_api1.farmily.com'; //PayPal API Username
$PayPalApiPassword  = '1398619284'; //Paypal API password
$PayPalApiSignature     = 'AHSJmRFeNsdM7JF5kqHVNYA-vnrsAk6.Y8bWB44l4JwWRnKTLJVj8TBv'; //Paypal API Signature
$PayPalCurrencyCode     = 'GBP'; //Paypal Currency Code
$PayPalReturnURL    = 'http://localhost/farmily/processpayment.php'; //Point to process.php page
$PayPalCancelURL    = 'http://localhost/farmily/processpayment.php'; //Cancel URL if user clicks cancel

// load classes

spl_autoload_register(function($class) {
	require_once 'classes/' . $class . '.php';
});

require_once 'functions/sanitize.php';

if(Cookie::exists(Config::get('remember/cookie_name')) && !Session::Exists(Config::get('session/session_name'))) {
	$hash = Cookie::Get(Config::get('remember/cookie_name'));
	$hashCheck = DB::getInstance()->get('user_session', array('hash', '=', $hash));

	if($hashCheck->count()) {
		$user = new User($hashCheck->first()->user_id);
		$user->login();

	}
}

?>