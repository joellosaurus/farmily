<!DOCTYPE html>

<!-- **** HEADER **** -->

<!-- IE9 Compatability: [if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Farmily</title>
  <!-- Foundation links -->
  <link rel="stylesheet" href="foundation-5.1.1/css/normalize.css">
  <link rel="stylesheet" href="foundation-5.1.1/css/foundation.css">
  <link rel="stylesheet" href="foundation-5.1.1/css/layout.css">
  <script src="foundation-5.1.1/js/vendor/modernizr.js"></script>
</head>
<body class="body-color">

<!-- **** TOP SECTION **** -->



<div class="row">
  <div class="header">
    <div class="small-3 columns logo">
      <img class="logo-img"/>
    </div>
    <div class="small-6 columns central">
    </div>

<?php
require_once 'core/init.php';
@ $db = new mysqli( "localhost", "joel", "lavatwilight4", "farmily");

?>


<?php
if(Session::exists('home')){
  echo '<p>' . Session::flash('home') . '</p>';
}

$user = new User();
if($user->isLoggedIn()) { 

  if($user->hasPermission('admin')) {
?>

<!-- Sign in/ sign up buttons-->

    <div class="small-3 columns tools">
      
        <?php
                  if($user->hasPermission('admin')) {
                    echo '<span class="label round success left">Administrator Account</span>';
                  

/* could also do

  if(!$user->hasPermission('admin')) {
    Redirect::('home');
  }

*/
        ?>

    <a href="profile.php?user=<?php echo htmlspecialchars($user->data()->userName); ?>" class="tiny round right alert button split"><?php echo htmlspecialchars($user->data()->fname); ?><span data-dropdown="drop"></span></a><br>
      <ul id="drop" class="f-dropdown" data-dropdown-content>
        <li><a href="addressview.php">View Address</a></li>
        <li><a href="addressupdate.php">Update Address</a></li>
        <li><a href="orderhistory.php">Order History</a></li>
        <li><a href="logout.php">Logout</a></li>
      </ul>


    </div>

<?php
} else {
  echo '<a href="register.php" class="signup-buttons button tiny right">Sign Up</a>
      <a href="logintest.php" data-reveal-id="signInModal" class="signup-buttons button tiny right">Sign In</a>
    </div>';
}
?>
      
  </div>

</div>
<!-- Nav bar -->

<div class="row">

  <?php
if(Input::exists()) {
  if (Token::check(Input::get('token'))) {

    $validate = new Validate();
    $validation = $validate->check($_POST, array(
      'userName' => array('required' => true),
      'password' => array('required' => true)
    ));

    if($validation->passed()) {
      // log user in
      $user = new User();
      $remember = (Input::get('remember') === 'on') ? true : false;
      $login = $user->login(Input::get('userName'), Input::get('password'), $remember);

      if($login) {

        Session::flash('home', '<div data-alert class="alert-box success radius popup">Logged In.<a href="#" class="close">&times;</a></div>');
        Redirect::to('index.php');
      } else {
        Session::flash('home', '<div data-alert class="alert-box alert radius popup">Your email address or password was incorrect, please try logging in again.<a href="#" class="close">&times;</a></div>');
        Redirect::to('index.php');
      }
    }
  }
}
?>

  <nav class="small-12 columns top-bar" data-topbar>
    <section class="top-bar-section">

<!-- Left Nav Section -->
      <ul class="left">
        <li><a href="index.php">Home</a></li>
        <li class="divider"></li>
        <li class="active has-dropdown">
          <a href="shop.php">Shop</a>
            <ul class="dropdown">
              <li><a href="food.php">Food</a></li>
              <li><a href="kitchenware.php">Kitchenware</a></li>
              <li><a href="pet.php">Pet</a></li>
              <li><a href="clothing.php">Clothing</a></li>
              <li><a href="other.php">Other</a></li>
            </ul>
        <li class="divider"></li>
        <li><a href="boxes.php">Boxes</a></li>
        <li class="divider"></li>
        <li><a href="recipes.php">Recipes</a></li>
        <li class="divider"></li>
        <li><a href="about.php">About</a></li>
        <li class="divider"></li>
        <li><a href="contact.php">Contact</a></li>
        <li class="divider"></li>

<!-- search section -->

      <li class="has-form">
        <div class="row collapse">
          <div class="large-8 small-9 columns">
            <input type="text" placeholder="Find Stuff">
          </div>
          <div class="large-4 small-3 columns">
          <a href="#" class="alert button expand">Search</a>
          </div>
        </div>
      </li>

    </section>
  </nav>
</div>

<!-- **** MIDDLE SECTION **** -->

<!--basket-->

<div class="row">
  <div class="small-3 columns left-block">
    <div class="basket">
    </div>

<!--shop navigation left-->

    <div class="shop-nav">
      <ul class="side-nav text-center">
        <li><a href="index.php">Home</a></li>
        <li><a href="shop.php">Shop</a></li>
        <li><a href="boxes.php">Boxes</a></li>
        <li><a href="recipes.php">Featured Recipes</a></li>
        <li><a href="mailinglist.php">Mailing List</a></li>
        <li><a href="contactus.php">Contact</a></li>
        <li><a href="contact.php">Find Us</a></li>
        <li><a href="about.php">About</a></li>
      </ul>
    </div>
  </div>

<!--shop category selection-->

<!--item detail-->

<div class="small-6 columns middle-block-scroll text-center">
<a href="manager.php" class="button tiny">Back</a>

  <h6>Add Item</h6>
  <!-- registration form -->
  <form data-abide action="" method="post">

  <div class="row">
    <div class="large-6 columns">

      <label for="itemname">Item Name
        <small>(required)</small>
        <input type="text" required pattern="[a-zA-Z]+" name="itemname" id="itemname" placeholder="Name">
      </label>
    </div>


    <div class="large-6 columns">
      <label for="category">Category
        <small>(required)</small>
        <input type="text" name="category" id="category" placeholder="Category"  >
      </label>
    </div>
  </div>

  <div class="row">
    <div class="large-6 columns">
      <label for="brand">Brand
        <small>(required)</small>
        <input type="text" name="brand" id="brand" placeholder="Brand"  >
      </label>
    </div>

    <div class="large-6 columns">
      <label for="description">Description
        <small>(required)</small>
        <input type="text" name="description" id="description" placeholder="Description"  >
      </label>
    </div>
  </div>

  <div class="row">
    <div class="large-4 columns">
      <label for="price">Price
        <small>(required)</small>
        <input type="text" name="price" id="price" placeholder="Price"  >
      </label>
    </div>

    <div class="large-4 columns">
      <label for="stock">Stock Level
        <small>(required)</small>
        <input type="text" name="stock" id="stock" placeholder="Stock Level"  >
      </label>
    </div>

    <div class="large-4 columns">
      <label for="availability">Available? y/n
        <small>(required)</small>
        <input type="text" name="availability" id="availability" placeholder="Enter y or n"  >
      </label>
    </div>
  </div>

  <div class="row">
    <div class="large-12 columns">
      <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
    <input type="submit" class="modal-button1 button expand" value="Add Item">
    </div>
  </div>

  <?php

if (isset($_POST['itemname'])) {
// escape variables for security
    $name = htmlspecialchars($_POST['itemname']);
    $category = htmlspecialchars($_POST['category']);
    $brand = htmlspecialchars($_POST['brand']);
    $description = htmlspecialchars($_POST['description']);
    $price = htmlspecialchars($_POST['price']);
    $stock = htmlspecialchars($_POST['stock']);
    $availability = htmlspecialchars($_POST['availability']);

    $query = "INSERT INTO items (itemName, itemCategory, brand, description, price, stock, availability)
    VALUES ('$name', '$category', '$brand', '$description', '$price', '$stock', '$availability')";

    //send the query to the db
    $db->query($query);
    $db->close();
   echo '<div data-alert class="alert-box success radius popup">Item Added.<a href="#" class="close">&times;</a></div>';

}



?>
</div>

 <div class="small-3 columns right-box">
  </div>

</div>

<!-- **** BOTTOM SECTION **** -->

<div class="row">
  <div class="small-4 columns skype">
    <div class="small-8 columns skype">
     
    </div>
    <div class="small-4 columns ">
    </div>
    </div>

  <div class="small-4 columns social">


    <div class="small-8 columns social-feed">
     <div>
      </div>

    </div>
  </div>

  <div class="small-4 columns tandc">

  </div>
</div>

<!-- OTHER FUNCTIONALITY -->

<!-- sign in modal -->
<div id="signInModal" class="modal-custom reveal-modal small" data-reveal>

  <h2>Sign In</h2>
  <p class="lead">Please enter your email address and password below to sign in.</p>
  <a class="close-reveal-modal">&#215;</a> 
  <!-- sign in form -->
  <form data-abide action="" method="post">
  <div class="row">
    <div class="large-12 columns">
      <label>Email
        <input type="text" name="userName" id="userName" autocomplete="off" placeholder="Email" required>
        <small class="error">Please enter your email address.</small>
      </label>
  </div>
</div>
  <div class="row">
    <div class="large-12 columns">
      <label>Password
        <input type="password" name="password" id="password" autocomplete="off" placeholder="Password" required pattern="[a-zA-Z]+">
        <small class="error">Please enter your password.</small>
      </label>
  </div>
</div>
<div class="row">
    <div class="large-4 columns left">
  <label for="remember">
      <input type="checkbox" name="remember" id="remember"> <small>Remember Me</small>
    </label>
      </div>
    </div>
<div class="row">
    <div class="large-12 columns">
      <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
      <input type="submit" name="login" class="modal-button1 button expand" value="Sign In">
    </div>
  </div>
<div class="row">
    <div class="large-12 columns">
      <a href="register.php" class="modal-button2 button tiny right">Not a Member? Sign Up!</a>
    </div>
  </div>

</form>
</div>

  

  <script src="foundation-5.1.1/js/vendor/jquery.js"></script>
  <script type="text/javascript" src="http://www.skypeassets.com/i/scom/js/skype-uri.js"></script>
  <script src="foundation-5.1.1/js/foundation.min.js"></script>
  <script>
    $(document).foundation();
  </script>

</body>
<?php      } else 
       Redirect::to('404.php');
} else 
       Redirect::to('404.php');
       ?>

</html>