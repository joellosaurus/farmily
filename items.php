<!DOCTYPE html>

<!-- **** HEADER **** -->

<!-- IE9 Compatability: [if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Farmily</title>
  <!-- Foundation links -->
  <link rel="stylesheet" href="foundation-5.1.1/css/normalize.css">
  <link rel="stylesheet" href="foundation-5.1.1/css/foundation.css">
  <link rel="stylesheet" href="foundation-5.1.1/css/layout.css">
  <script src="foundation-5.1.1/js/vendor/modernizr.js"></script>
  <meta name="google-translate-customization" content="349ee5e8b83b03a6-cc950e275581b1ad-g179f3e864b250654-1b"></meta>  
</head>
<body class="body-color">

<!-- **** TOP SECTION **** -->



<div class="row">
  <div class="header">
    <div class="small-3 columns logo">
      <img class="logo-img"/>
    </div>
    <div class="small-6 columns central">
    </div>

<?php
require_once 'core/init.php';

?>


<?php
if(Session::exists('home')){
  echo '<p>' . Session::flash('home') . '</p>';
}

$user = new User();
if($user->isLoggedIn()) { 
?>

<!-- Sign in/ sign up buttons-->

    <div class="small-3 columns tools">
      
        <?php
                  if($user->hasPermission('admin')) {
                    echo '<span class="label round success left">Administrator Account</span>';
                  }

/* could also do

  if(!$user->hasPermission('admin')) {
    Redirect::('home');
  }

*/
        ?>

    <a href="profile.php?user=<?php echo htmlspecialchars($user->data()->userName); ?>" class="tiny round right alert button split"><?php echo htmlspecialchars($user->data()->fname); ?><span data-dropdown="drop"></span></a><br>
      <ul id="drop" class="f-dropdown" data-dropdown-content>
        <li><a href="addressview.php">View Address</a></li>
        <li><a href="addressupdate.php">Update Address</a></li>
        <li><a href="orderhistory.php">Order History</a></li>
        <li><a href="logout.php">Logout</a></li>
      </ul>


    </div>

<?php
} else {
  echo '<a href="register.php" class="signup-buttons button tiny right">Sign Up</a>
      <a href="logintest.php" data-reveal-id="signInModal" class="signup-buttons button tiny right">Sign In</a>
    </div>';
}
?>
      
  </div>

</div>
<!-- Nav bar -->

<div class="row">

  <?php
if(Input::exists()) {
  if (Token::check(Input::get('token'))) {

    $validate = new Validate();
    $validation = $validate->check($_POST, array(
      'userName' => array('required' => true),
      'password' => array('required' => true)
    ));

    if($validation->passed()) {
      // log user in
      $user = new User();
      $remember = (Input::get('remember') === 'on') ? true : false;
      $login = $user->login(Input::get('userName'), Input::get('password'), $remember);

      if($login) {

        Session::flash('home', '<div data-alert class="alert-box success radius popup">Logged In.<a href="#" class="close">&times;</a></div>');
        Redirect::to('index.php');
      } else {
        Session::flash('home', '<div data-alert class="alert-box alert radius popup">Your email address or password was incorrect, please try logging in again.<a href="#" class="close">&times;</a></div>');
        Redirect::to('index.php');
      }
    }
  }
}
?>

  <nav class="small-12 columns top-bar" data-topbar>
    <section class="top-bar-section">

<!-- Left Nav Section -->
      <ul class="left">
        <li><a href="index.php">Home</a></li>
        <li class="divider"></li>
        <li class="active has-dropdown">
          <a href="shop.php">Shop</a>
            <ul class="dropdown">
              <li><a href="food.php">Food</a></li>
              <li><a href="kitchenware.php">Kitchenware</a></li>
              <li><a href="pet.php">Pet</a></li>
              <li><a href="clothing.php">Clothing</a></li>
              <li><a href="other.php">Other</a></li>
            </ul>
        <li class="divider"></li>
        <li><a href="boxes.php">Boxes</a></li>
        <li class="divider"></li>
        <li><a href="recipes.php">Recipes</a></li>
        <li class="divider"></li>
        <li><a href="about.php">About</a></li>
        <li class="divider"></li>
        <li><a href="contact.php">Contact</a></li>
        <li class="divider"></li>

<!-- search section -->

      <li class="has-form">
        <div class="row collapse">
          <div class="translate">
            <div id="google_translate_element"></div><script type="text/javascript">
              function googleTranslateElementInit() {
                new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'de,en,es,fr,sv', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
              }
             </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
          </div>
        </div>
      </li>

    </section>
  </nav>
</div>

<!-- **** MIDDLE SECTION **** -->

<!--basket-->

<div class="row">
  <div class="small-3 columns left-block">
    <div class="basket">
      <b>Basket</b><br>
          <?php

          // if "products" session exists, show items and links. Else, echo cart is empty.

          if(isset($_SESSION["products"]))
          {
              $total = 0;
              echo '<ol>';
              foreach ($_SESSION["products"] as $cart_itm)
              {
                  echo '<li class="cart-itm">';
                  echo '<span class="remove-itm"><a href="cart_update.php?removep='.$cart_itm["code"].'&return_url='.$current_url.'">&times;</a></span>';
                  echo '<br><h7>'.$cart_itm["name"].'</h7>';
                  echo '<div class="p-code">Code : '.htmlspecialchars($cart_itm["code"]).'</div>';
                  echo '<div class="p-qty">Qty : '.htmlspecialchars($cart_itm["qty"]).'</div>';
                  echo '<div class="p-price">Price: '. $currency . htmlspecialchars($cart_itm["price"]).'</div>';
                  echo '</li>';
                  $subtotal = ($cart_itm["price"]*$cart_itm["qty"]);
                  $total = ($total + $subtotal);
              }
                  echo '</ol>';
                  echo '<span class="check-out-txt"><strong>Total : '.$currency.$total.'</strong> <a href="viewbasket.php">Check Out! </a></span>';
                  echo '|';
                  echo '<span class="empty-cart"><a href="cart_update.php?emptycart=1&return_url='.$current_url.'"> Empty</a></span>';
              }else{
                  echo 'Your basket is empty';
              }
          ?>
    </div>

<!--shop navigation left-->

    <div class="shop-nav">
      <ul class="side-nav text-center">
        <li><a href="index.php">Home</a></li>
        <li><a href="shop.php">Shop</a></li>
        <li><a href="boxes.php">Boxes</a></li>
        <li><a href="recipes.php">Featured Recipes</a></li>
        <li><a href="mailinglist.php">Mailing List</a></li>
        <li><a href="contactus.php">Contact</a></li>
        <li><a href="contact.php">Find Us</a></li>
        <li><a href="about.php">About</a></li>
      </ul>
    </div>
  </div>

<!--shop category selection-->

<!--item detail-->

<div class="small-6 columns middle-block-scroll">
    <a onclick="history.go(-1);" class="button tiny">Back</a><br>
   
<?php 

// with help from week 7 lecture notes

// database

 @ $db = new mysqli( "localhost", "joel", "lavatwilight4", "farmily");
        //check the connection
        if (mysqli_connect_errno()) {
            echo 'Error: Could not connect to database.  Please try again later.';
            exit;
        }
        else {

          // if no post value, redirect
/*
          if(!isset($_POST["type"])) {
            header( 'Location: shop.php' ) ;;
          } else {
*/
          if(isset($_POST["type"])){
                    $category = $_POST["type"];;
                    $_SESSION["type"] = $_POST["type"];
            } else {
              //if no post data recieved, take session
              $category = $_SESSION["type"];
            }
          
        
          
          echo "You are viewing all <b>". $category . "</b> products in our store.";
        }
      
        
// query
if(isset($_SESSION['type'])){
        $query = "SELECT * FROM items WHERE itemCategory='$category' AND availability='y'";
        
        //send the query to the db
        $result = $db->query($query);
        $noOfRows = $result->num_rows;
        
        // create the table
        print ("<form action='ItemDetail.php' method='post'><table class='table-style' border='3'");
        
        // add the rows to the table
        for($i = 0; $i < $noOfRows; $i++ ) {
            
            $row = $result->fetch_assoc();
            
            //add a row
            print ("<tr>");
     //  <input class="item-cat-img" name="type" value="Vegetable Box" type="image" src="assets/images/boxes/vegetable.png" alt="Submit">
           
            //print a cell
            print ("<td> <img src=$row[picture] width='75' height='75'> </td>");
            print ("<td> £$row[price] </td>");
            print ("<td><input class='text-item-link' name='item' value='$row[itemName]' type='image'></td>");

            //close row
            print ("</tr>");
            
        }
        
        //close table
        print ("</table></form>");
        
        $result->free();
        $db->close();
      }
        
?>  

</div>

 <div class="small-3 columns right-box">
  </div>

</div>

<!-- **** BOTTOM SECTION **** -->

<div class="row">
  <div class="large-4 small-12 columns skype">
    <div class="small-4 columns skype-img">
          <br>
          <br>
          <br>
          <br>
          <br>
             <div id="SkypeButton_Call_joellodactyl_1">
              <script type="text/javascript" src="http://www.skypeassets.com/i/scom/js/skype-uri.js"></script>
                <div id="SkypeButton_Call_FarmilyShop_1">
                  <script type="text/javascript">
                    Skype.ui({
                          "name": "dropdown",
                          "element": "SkypeButton_Call_FarmilyShop_1",
                          "participants": ["FarmilyShop"],
                          "imageSize": 14
                            });
                  </script>
                </div>
              </script>
            </div>
    </div>

  <div class="small-8 columns skype-info">
  </div>
</div>


  <div class="small-4 columns social">
    <div class="small-4 columns social-buttons">
      <div>
        <a href="http://www.facebook.com"><img src="assets/images/facebookicon.png" width="55" height="60"></a>
        <a href="http://plus.google.com"><img src="assets/images/googleplusicon.png" width="55" height="60"></a>
        <a href="http://www.twitter.com/farmilyshop"><img src="assets/images/twittericon.png" width="55" height="60"></a>
        <a href="http://www.instagram.com"><img src="assets/images/instagramicon.png" width="55" height="60"></a>
      </div>
    </div>

    <div class="small-8 columns social-feed">
     <div> <a class="twitter-timeline" href="https://twitter.com/FarmilyShop" data-widget-id="451361322781769729"></a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
      </div>

    </div>
  </div>

  <div class="small-4 columns tandc">
    <div class="small-4 columns tandc-menu">
    </div>
    <div class="small-8 columns partners">
    </div>
  </div>
</div>

<!-- OTHER FUNCTIONALITY -->

<!-- sign in modal -->
<div id="signInModal" class="modal-custom reveal-modal small" data-reveal>

  <h2>Sign In</h2>
  <p class="lead">Please enter your email address and password below to sign in.</p>
  <a class="close-reveal-modal">&#215;</a> 
  <!-- sign in form -->
  <form data-abide action="" method="post">
  <div class="row">
    <div class="large-12 columns">
      <label>Email
        <input type="text" name="userName" id="userName" autocomplete="off" placeholder="Email" required>
        <small class="error">Please enter your email address.</small>
      </label>
  </div>
</div>
  <div class="row">
    <div class="large-12 columns">
      <label>Password
        <input type="password" name="password" id="password" autocomplete="off" placeholder="Password" required pattern="[a-zA-Z]+">
        <small class="error">Please enter your password.</small>
      </label>
  </div>
</div>
<div class="row">
    <div class="large-4 columns left">
  <label for="remember">
      <input type="checkbox" name="remember" id="remember"> <small>Remember Me</small>
    </label>
      </div>
    </div>
<div class="row">
    <div class="large-12 columns">
      <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
      <input type="submit" name="login" class="modal-button1 button expand" value="Sign In">
    </div>
  </div>
<div class="row">
    <div class="large-12 columns">
      <a href="register.php" class="modal-button2 button tiny right">Not a Member? Sign Up!</a>
    </div>
  </div>

</form>
</div>

  

  <script src="foundation-5.1.1/js/vendor/jquery.js"></script>
  <script type="text/javascript" src="http://www.skypeassets.com/i/scom/js/skype-uri.js"></script>
  <script src="foundation-5.1.1/js/foundation.min.js"></script>
  <script>
    $(document).foundation();
  </script>

</body>
</html>