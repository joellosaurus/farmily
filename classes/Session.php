<?php

class Session {

// if session exists return true, else false.

	public static function exists($name) {
		return (isset($_SESSION[$name])) ? true : false;
	}

// put token

	public static function put($name, $value) {
		return $_SESSION[$name] = $value;
	}

// get value

	public static function get($name) {
		return $_SESSION[$name];
	}

// delete token

	public static function delete ($name) {
		if(self::exists($name)) {
			unset($_SESSION[$name]);
		}
	}

// flash function to set messages, and delete the message when refresh.

	public static function flash($name, $string = '') {
		if(self::exists($name)) {
			$session = self::get($name);
			self::delete($name);
			return $session;
		} else {
			self::put($name, $string);
		}
	}
}

?>