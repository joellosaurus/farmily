<?php

//database wrapper, PDO used
//singleton method

class DB {

	// store instance of database if available
	// underscores dicate private properties

	private static $_instance = null;
	private $_pdo, 
			$_query, 
			$_error = false, 
			$_results, 
			$_count = 0;


	// connect to database. Runs when class is called.
	// try and catch block to catch errors.

	private function __construct() {
		try {
			$this->_pdo = new PDO('mysql:host=' . Config::get('mysql/host') . ';dbname=' . Config::get('mysql/db'), Config::get('mysql/username'), Config::get('mysql/password'));
		} catch(PDOException $e) {
			die($e->getMessage());
		}
	}

// if already running, do not reconnect

	public static function getInstance() {
		if(!isset(self::$_instance)) {
			self::$_instance = new DB();
		}
		return self::$_instance;
	}

	// query method

	// binding paramaters, removes possibility of SQL injection.

	public function query($sql, $params = array()) {
		$this->_error = false;
		if($this->_query = $this->_pdo->prepare($sql)) {
			$x = 1;
			if(count($params)) {
				foreach($params as $param) {
					$this->_query->bindValue($x, $param);
					$x++;
				}
			}
			// if query successful (e.g database and fields actually exist) store results
			if($this->_query->execute()) {
				$this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
				$this->_count = $this->_query->rowCount();
				} else {
					$this->_error = true;
				}
		}
		return $this;

	}

// action function
	public function action($action, $table, $where = array()) {
		// need a field (e.g userName), operator (e.g =) and a value (e.g the name).
		if(count($where) === 3) {
			// operators allowed
			$operators = array('=', '>','<', '>=', '<=');

			$field 		= $where[0];
			$operator 	= $where[1];
			$value 		= $where[2];
		// if operators used are permited
		if(in_array($operator, $operators)) {
			$sql = "{$action} FROM {$table} WHERE {$field} {$operator} ?";
			
			if(!$this->query($sql, array($value))->error()) {
			return $this;
			}
			}
		}
		return false;
	}

//get function
	public function get($table, $where) {
	// select results
		return $this->action('SELECT *', $table, $where);
	}

	public function delete($table, $where) {
		return $this->action('DELETE', $table, $where);
	}

// insert into database (e.g user info)

    public function insert($table, $fields = array()) {
        if(count($fields)) {
            $keys = array_keys($fields);
            $values = '';
            $x = 1;

            foreach($fields as $field) {
                $values .= "?";
                if($x < count($fields)) {
                    $values .= ', ';
            }
            $x++;
        }

        $sql = "INSERT INTO {$table} (`" . implode('`, `', $keys) . "`) VALUES ({$values})";
        
        if(!$this->query($sql, $fields)->error()) {
            return true;
        }
    }
    return false;
}

// update fields
    public function update($table, $id, $fields) {
        $set = '';
        $x = 1;

        foreach($fields as $name => $value) {
            $set .= "{$name} = ?";
            if($x < count($fields)) {
                $set .= ', ';
            }
            $x++;
        }

        $sql = "UPDATE {$table} SET {$set} WHERE id = {$id}";

        if(!$this->query($sql, $fields)->error()) {
            return true;
        }
        return false;
    }

    // results
    public function results() {
        return $this->_results;
    }

// first result of query
    public function first(){
        return $this->results()[0];
    }

    // return error (so query could be if($user->error()))
	public function error() {
		return $this->_error; 
	}

	public function count() {
		return $this->_count;
	}
}





?>