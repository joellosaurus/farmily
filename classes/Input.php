<?php

class Input{

// post is default of no $type provided
	public static function exists($type = 'post') {
		// change $type if
		switch($type) {
			case 'post':
				return (!empty($_POST)) ? true : false;
			break;
			case 'get':
				return (!empty($_GET)) ? true : false;
			break;
			default:
				return false;
			break;
		}
	}

// get data from get or post

	public static function get($item) {
		if(isset($_POST[$item])) {
			return $_POST[$item];
		} else if (isset($_GET[$item])) {
			return $_GET[$item];
		}

		// if data doesnt exists, return something .
			return '';
	}
}

// check if data exists