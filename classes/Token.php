<?php

// token class allows generation of token
// checks if token is valid and exists, deletes existing token
// token generate for each refresh of the page


class token {

// generate token

	public static function generate() {
		return Session::put(Config::get('session/token_name'), md5(uniqid()));
	}

// check token exists, delete if it does
	
	public static function check($token) {
		$tokenName = Config::get('session/token_name');

		if(Session::exists($tokenName) && $token === Session::get($tokenName)) {
			Session::delete($tokenName);
			return true;
		}

		return false;
	}
}

?>