<?php


class Mailinglist {
	private $_db,
			$_data,
			$_sessionName,
			$_cookieName,
			$_isLoggedIn;


	public function __construct($mailingList = null) {
		$this->_db = DB::getInstance();

		$this->_sessionName = Config::get('session/session_name');
		$this->_cookieName = Config::get('remember/cookie_name');

		if(!$mailingList) {
			if(Session::exists($this->_sessionName)) {
				$mailingList = Session::get($this->_sessionName);
				if($this->find($mailingList)) {
					$this->_isLoggedIn = true;
				} else {
				//process logout
				}
			}
		}  else {
			$this->find($mailingList);
		}
	}

	public function update($fields = array(), $id = null){

		if(!$id && $this->isLoggedIn()) {
			$id = $this->data()->id;
		}

		if(!$this->_db->update('mailingList', $id, $fields)) {
			throw new Exception('There was a problem updating.');
		}

	}

	public function create($fields = array()) {
		if(!$this->_db->insert('mailingList', $fields)) {
			throw new Exception('There was a problem creating an account');
		}
	}

	public function find($mailingList = null) {
		if($mailingList) {
			$field = (is_numeric($mailingList)) ? 'id' : 'name';
			$data = $this->_db->get('mailingList', array($field, '=', $mailingList));

			if($data->count()) {
				$this->_data = $data->first();
				return true;
			}
		}
		return false;
	}

	public function login($mailingListname = null, $password = null, $remember = false) {

		if(!$mailingListname && !$password && $this->exists()) {
			Session::put($this->_sessionName, $this->data()->id);
		} else {
		$mailingList = $this->find($mailingListname);


			if($mailingList) {
				if($this->data()->password === Hash::make($password, $this->data()->salt)) {
					Session::put($this->_sessionName, $this->data()->id);

				// if checkbox is ticked, below block is run.
				if($remember) {
					$hash = Hash::unique();
					$hashCheck = $this->_db->get('user_session', array('user_id', '=', $this->data()->id));

					if(!$hashCheck->count()) {
						$this->_db->insert('user_session', array(
							'user_id' => $this->data()->id,
							'hash' => $hash
							));
						} else {
							$hash = $hashCheck->first()->hash;
						}

						Cookie::put($this->_cookieName, $hash, Config::get('remember/cookie_expiry'));
					}

					return true;

				}
			}
		}

		return false;
	}

	public function hasPermission($key) {
		$group = $this->_db->get('groups', array('id', '=', $this->data()->group));

		if($group->count()) {
			//decode jason from db, turn into array.
			$permissions = json_decode($group->first()->permissions, true);

			if($permissions[$key] == true) {
				return true;
			}
		}
		return false;
	}

	public function exists() {
		return (!empty($this->_data)) ? true : false;
	}

	public function logout(){

//delete has from database
		$this->_db->delete('user_session', array('user_id', '=', $this->data()->id));
//delete session and cookies
		Session::delete($this->_sessionName);
		Cookie::delete($this->_cookieName);
	}

	public function data(){
		return $this->_data;
	}

	public function isLoggedIn() {
		return $this->_isLoggedIn;
	}
}


?>