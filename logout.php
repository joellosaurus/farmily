<?php
require_once 'core/init.php';

$user = new User();
$user->logout();

Session::flash('home', '<div data-alert class="alert-box alert radius popup">You have logged out.<a href="#" class="close">&times;</a></div>');
Redirect::to('index.php');
?>